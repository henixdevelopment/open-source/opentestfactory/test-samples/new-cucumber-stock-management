Project using Cucumber > 4.

A branch exists for each major version of Cucumber.

Quick explanations on the Parameters.feature:

@Community

Will assert the following operation: 

(DS_warehouse_capacity > (DS_current_stock + TC_CUF_estimated_production))

@Premium

CPG_CUF_product_value_is_five : (bool) assertTrue on the value

IT_CUF_warehouse_name : (str) should be equal to "WH1948"

Will assert the following operation: 

((DS_current_stock * 5) / 3 ) > (DS_renting_cost + TS_CUF_maintenance_cost)

@ref![#12+1.5~]/lin-_={%?}:verif

`DS_product_reference` : `[#P15@{10}]Box/\Panel;`

`DS_product_description` : `Th|s s'tack* has : mul_ti-ple boxes ?! & pa"nels (%).`